$('.js-catalog').each(function () {
    var $wrap        = $(this);
    var $element     = $wrap.find('.js-catalog-element');
    var $btnMore     = $element.find('.js-catalog-btn-more');
    var $description = $element.find('.js-catalog-description');



    $btnMore.on('click', function (e) {
        e.preventDefault();

        $(this).closest($element).toggleClass('active').siblings().removeClass('active');

        $element.each(function () {
            if ($(this).hasClass('active')) {
                $(this).find($description).slideDown(300);
            } else {
                $(this).find($description).slideUp(300);
            }
        });
    });
});