$('form').on('submit', function () {
    var $wrap            = $(this),
        $requiredEntries = $wrap.find('[required]'),
        continueSubmit   = true,
        formData         = new FormData($wrap[0]);



    $requiredEntries.each(function () {
        if ($(this).val() == '') {
            continueSubmit = false;
            alert('Пожалуйста, заполните форму!');
            return false;
        }
    });

    if (!continueSubmit) {
        return false;
    }

    $.ajax({
        url: 'php/main.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function () {
            $wrap.trigger('reset');

            $('.overlay, .js-pop-up').hide();
            centerPopUp('.js-pop-up-thanks');
            $('.overlay, .js-pop-up-thanks').fadeIn(150);

            setTimeout(function () {
                $('.overlay, .js-pop-up-thanks').fadeOut(150);
            }, 5000);
        }
    }).fail(function () {
        alert('Извините, данные не были переданы!');
    });

    return false;
});